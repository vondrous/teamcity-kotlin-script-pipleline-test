
import jetbrains.buildServer.configs.kotlin.v2018_2.BuildType
import jetbrains.buildServer.configs.kotlin.v2018_2.DslContext
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.gradle
import jetbrains.buildServer.configs.kotlin.v2018_2.project
import jetbrains.buildServer.configs.kotlin.v2018_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2018_2.version

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2018.2"

project {

    buildType(Build)
    buildType(UnitTest)
    buildType(Version)
    buildType(PostStep)
    buildType(PostStep2)
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "Gradle build"
            tasks = "build"
            buildFile = "build.gradle.kts"
            gradleParams = "-x test"
        }
    }

    triggers {
        vcs {
        }
    }

    dependencies {
        snapshot(Version) {
        }
    }
})

object UnitTest : BuildType({
    name = "Unit Test"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "Unit test"
            tasks = "test"
            buildFile = "build.gradle.kts"
        }
    }

    triggers {
        vcs {
        }
    }

    dependencies {
        snapshot(Build) {
            runOnSameAgent = true
        }
    }
})

object Version : BuildType({
    name = "Version"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "Gradle Version"
            buildFile = "build.gradle.kts"
            gradleParams = "--version"
        }
        gradle {
            name = "Run help"
            tasks = "help"
            buildFile = "build.gradle.kts"
        }
    }

    triggers {
        vcs {
        }
    }
})

object PostStep : BuildType({
    name = "PostStep"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "Gradle Post Tasks"
            tasks = "tasks"
            buildFile = "build.gradle.kts"
        }
    }

    triggers {
        vcs {
        }
    }

    dependencies {
        snapshot(UnitTest) {
        }
    }
})


object PostStep2 : BuildType({
    name = "PostStep2"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        gradle {
            name = "Gradle Post 2 Tasks"
            tasks = "tasks"
            buildFile = "build.gradle.kts"
        }
    }

    triggers {
        vcs {
        }
    }
    

    dependencies {
        snapshot(PostStep) {
        }
    }
})



