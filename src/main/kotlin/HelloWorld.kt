import java.time.LocalDateTime
import java.time.format.DateTimeFormatter.ofPattern

fun main() {
    println("Hello Kotlin project!")
}

class HelloWorld {
    fun getActualHello() = "Hello at ${LocalDateTime.now().format(ofPattern("HH:mm"))}"
}