import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test

internal class HelloWorldTest {

    @Test
    fun `getActualHello contains greeting`() {
        //when
        val result = HelloWorld().getActualHello()

        //then
        assertThat(result)
                .contains("Hello at ")
    }


    @Test
    fun `getActualHello contains date`() {
        //when
        val result = HelloWorld().getActualHello()

        //then
        assertThat(result)
                .containsPattern("[0-9]:[0-9]")

    }


    @Test
    fun `test added in Branch 1`() {
        //when
        println("Test works.")

        //then
        if (true) {
            println("Test works like a charm.")
        } else {
            assertThat(1).isEqualTo(2)
        }


    }
}